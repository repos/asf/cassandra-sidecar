/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.cassandra.sidecar.db;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.cassandra.sidecar.common.request.Service;
import org.apache.cassandra.sidecar.common.server.CQLSessionProvider;
import org.apache.cassandra.sidecar.db.schema.ConfigsSchema;
import org.apache.cassandra.sidecar.db.schema.SidecarSchema;

/**
 * {@link CdcConfigAccessor} is an accessor class for updating CDC configurations into
 * "configs" table in sidecar keyspace. All the cdc configurations required for CDC feature are
 * stored in this table using this class.
 */
@Singleton
public class CdcConfigAccessor extends ConfigAccessorImpl
{
    @Inject
    protected CdcConfigAccessor(ConfigsSchema configsSchema,
                                CQLSessionProvider sessionProvider,
                                SidecarSchema sidecarSchema)
    {
        super(configsSchema, sessionProvider, sidecarSchema);
    }

    @Override
    public Service service()
    {
        return Service.CDC;
    }
}
