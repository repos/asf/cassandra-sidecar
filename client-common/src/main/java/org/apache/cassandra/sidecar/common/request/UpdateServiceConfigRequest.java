/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.cassandra.sidecar.common.request;

import io.netty.handler.codec.http.HttpMethod;
import org.apache.cassandra.sidecar.common.ApiEndpointsV1;
import org.apache.cassandra.sidecar.common.request.data.UpdateCdcServiceConfigPayload;

/**
 * Request class for updating the config of a given service.
 * Ex: updating kafka configs or updating cdc configs
 */
public class UpdateServiceConfigRequest extends JsonRequest<UpdateCdcServiceConfigPayload>
{
    final UpdateCdcServiceConfigPayload payload;

    public UpdateServiceConfigRequest(Service service, UpdateCdcServiceConfigPayload payload)
    {
        super(ApiEndpointsV1.SERVICE_CONFIG_ROUTE.replaceAll(ApiEndpointsV1.SERVICE_PARAM, service.serviceName));
        this.payload = payload;
    }

    @Override
    public Object requestBody()
    {
        return this.payload;
    }

    @Override
    public HttpMethod method()
    {
        return HttpMethod.PUT;
    }
}
